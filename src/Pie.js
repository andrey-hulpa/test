import React from 'react';
import PieChart from 'react-minimal-pie-chart';



class PieExample extends PieChart {

  render() {
    return (
    <PieChart  data = {this.props.data}/>
    );
  }
}

export default PieExample;
