import React, { Component } from 'react';
import PieExample from './Pie.js';

import './App.css';

const list = [
  {
    id: 0,
    title: 'John Lennon',
    value: 11,
    color: '#E38627'
  },
  {
    id: 1,
    title: 'Paul McCartney',
    value: 15,
    color: '#C13C37'
  }
];

class Vote extends Component {
  state = {
    members: []
  };

  componentDidMount() {
    this.setState({ members: list });
  }

  handleEvent = (memberId) => {
    const updatedList = this.state.members.map((member) => {
      if (member.id === memberId) {
        return Object.assign({}, member, {
          value: member.value + 1
        });
      } else {
        return member;
      }
    });

    this.setState({
      members: updatedList
    });
  };

  render() {
    return this.state.members.map((member) => (
      <Voting
        key={member.id}
        id={member.id}
        title={member.title}
        votes={member.value}
        onVote={this.handleEvent}
      />
    ));
  }
}
class Voting extends Component {
  handleClick = () => this.props.onVote(this.props.id);

  render() {
    return (
      <div className="App">
        <span onClick={this.handleClick}> {this.props.title}</span>
        {this.props.votes}
      </div>
    );
  }
}

class App extends Component {
  render() {
    return (
      <div>
        <Vote />
        <span id="status"> </span>
        <PieExample data={list} />
      </div>
    );
  }
}
export default App;
